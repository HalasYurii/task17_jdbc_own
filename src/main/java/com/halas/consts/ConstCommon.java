package com.halas.consts;

import java.util.Scanner;

public class ConstCommon {
    public static final String DEFAULT_DATE = "1000-10-10";
    public static final String SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION =
            "Duplicate entry, ERROR SQLIntegrityConstraintViolationException.\n\n";

    public static final String COUNT_CREATED_ROWS = "Count created rows: ";
    public static final String COUNT_UPDATED_ROWS = "Count updated rows: ";
    public static final String COUNT_DELETED_ROWS = "Count deleted rows: ";
    public static final String CREATE_INFO = "****CREATE****";
    public static final String UPDATE_INFO = "****UPDATE****";
    public static final String DELETE_INFO = "****DELETE****";
    public static final String FIND_INFO = "****FIND****";

    public static final String TABLE_CONSUMER = "Table: consumer.";
    public static final String TABLE_CONTRACT = "Table: contract.";
    public static final String TABLE_COUNTRY = "Table: country.";
    public static final String TABLE_MOBILE_NET = "Table: mobile_network.";
    public static final String TABLE_MOBILE_OPER = "Table: mobile_operator.";
    public static final String TABLE_NUMBER = "Table: number.";
    public static final String TABLE_PIN_CODE = "Table: pin_code.";
    public static final String TABLE_TARIFF = "Table: tariff.";
    public static final String TABLE_TARIFF_HMO = "Table: tariff_has_mobile_operator.";

    public static final String INPUT_PK = "Input primary key (%s).";
    public static final String EMPTY_TABLE = "Empty.. there is nothing to show..";

    public static final String BAD_PK_SHOW = "Can't find by this PK.\n\n";
    public static final String INTEGER_MSG = "integer";
    public static final String DOUBLE_MSG = "double";
    public static final String STRING_MSG = "string";
    public static final String DATE_MSG = "date";
    public static final String BIG_DECIMAL_MSG = "bigDecimal";

    public static final Scanner INPUT = new Scanner(System.in);

    private ConstCommon() {
    }
}
