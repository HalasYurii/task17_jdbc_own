package com.halas.consts;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConstConnection {
    private static final String URL =
            "jdbc:mysql://localhost:3306/mobile_communication?serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";
    private static final String USER = "root";
    private static final String PASSWORD = "423489123789op";
    private static Connection connInit;
    static {
        try {
            connInit = DriverManager.getConnection(URL, USER, PASSWORD);
        } catch (SQLException ignored) {
        }
    }
    public static final Connection CONNECTION = connInit;

    private ConstConnection(){
    }
}
