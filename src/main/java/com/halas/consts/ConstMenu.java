package com.halas.consts;

public class ConstMenu {
    public static final String SQL_EXCEPTION =
            "\nSQL can't do current action, sorry..\nSQLException!\n\n";
    public static final String SELECT_MSG =
            "Please, select menu point.";
    public static final String TRY_AGAIN =
            "Bad inputted value!\nPlease try again..";

    public static final String EXIT_POINT = "0";
    public static final String BACK_POINT = "0";
    public static final String MENU_START = "***START MENU***";
    public static final String MENU_CONSUMER = "***CONSUMER MENU***";
    public static final String MENU_CONTRACT = "***CONTRACT MENU***";
    public static final String MENU_COUNTRY = "***COUNTRY MENU***";
    public static final String MENU_MOBILE_NETWORK = "***MOBILE NETWORK MENU***";
    public static final String MENU_MOBILE_OPERATOR = "***MOBILE OPERATOR MENU***";
    public static final String MENU_NUMBER = "***NUMBER MENU***";
    public static final String MENU_PIN_CODE = "***PIN CODE MENU***";
    public static final String MENU_TARIFF = "***TARIFF MENU****";
    public static final String MENU_TARIFF_HMO = "***TARIFF HAS MOBILE OPERATOR MENU***";

    private ConstMenu() {
    }
}
