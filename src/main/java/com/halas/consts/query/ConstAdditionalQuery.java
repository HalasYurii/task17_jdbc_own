package com.halas.consts.query;

public class ConstAdditionalQuery {
    public static final String NUMBER_MOBILE_OPERATOR_CONSUMER_ALL = "SELECT\n" +
            " consumer.code,consumer.name,consumer.surname,consumer.date_born,consumer.country_born,consumer.money,consumer.type_money,\n" +
            " number.mobile,number.consumer_code,number.contract_code,number.pin_code,number.mobile_operator_code,\n" +
            " mobile_operator.code,mobile_operator.name,mobile_operator.country_code,mobile_operator.mobile_network_generation\n" +
            " FROM consumer JOIN number JOIN mobile_operator\n" +
            " WHERE number.consumer_code LIKE consumer.code AND\n" +
            "\tnumber.mobile_operator_code LIKE mobile_operator.code";

    public static final String PIN_CODE_AND_CODE_OPERATOR = "SELECT DISTINCT pin_code, mobile_operator_code\n" +
            " FROM number\n" +
            " WHERE pin_code=? AND mobile_operator_code=?";


    public static final String SUCCESS = "Successfully, congratulations!";
    public static final String FAIL = "FAIL, but don't give up..";
    private ConstAdditionalQuery() {
    }
}
