package com.halas.consts.query;

public class ConstConsumerQuery {
    public static final String FIND_ALL_CONSUMER =
            "SELECT code, name, surname, date_born, country_born, money, type_money FROM consumer";
    public static final String FIND_BY_PK =
            "SELECT code, name, surname, date_born, country_born, money, type_money FROM consumer WHERE code=?";
    public static final String DELETE_BY_PK = "DELETE FROM consumer WHERE code=?";
    public static final String CREATE =
            "INSERT consumer (code, name, surname, date_born, country_born, money, type_money) VALUES (?, ?, ?, ?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE consumer SET name=?, surname=?, date_born=?, country_born=?, money=?, type_money=? WHERE code=?";

    private ConstConsumerQuery() {
    }
}
