package com.halas.consts.query;

public class ConstContractQuery {
    public static final String FIND_ALL =
            "SELECT code, date_start, date_end, tariff_code FROM contract";
    public static final String FIND_BY_PK =
            "SELECT code, date_start, date_end, tariff_code FROM contract WHERE code=?";
    public static final String DELETE_BY_PK = "DELETE FROM contract WHERE code=?";
    public static final String CREATE =
            "INSERT contract (code, date_start, date_end, tariff_code) VALUES (?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE contract SET date_start=?, date_end=?, tariff_code=? WHERE code=?";

    private ConstContractQuery() {
    }
}
