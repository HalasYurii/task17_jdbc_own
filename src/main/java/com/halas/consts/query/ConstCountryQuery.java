package com.halas.consts.query;

public class ConstCountryQuery {
    public static final String FIND_ALL =
            "SELECT code, name FROM country";
    public static final String FIND_BY_PK =
            "SELECT code, name FROM country WHERE code=?";
    public static final String DELETE_BY_PK = "DELETE FROM country WHERE code=?";
    public static final String CREATE =
            "INSERT country (code, name) VALUES (?, ?)";
    public static final String UPDATE =
            "UPDATE country SET name=? WHERE code=?";

    private ConstCountryQuery() {
    }
}
