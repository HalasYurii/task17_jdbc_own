package com.halas.consts.query;

public class ConstMobileNetworkQuery {
    public static final String FIND_ALL =
            "SELECT generation, speed, type_speed, year_born FROM mobile_network";
    public static final String FIND_BY_PK =
            "SELECT generation, speed, type_speed, year_born FROM mobile_network WHERE generation=?";
    public static final String DELETE_BY_PK = "DELETE FROM mobile_network WHERE generation=?";
    public static final String CREATE =
            "INSERT mobile_network (generation, speed, type_speed, year_born) VALUES (?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE mobile_network SET speed=?, type_speed=?, year_born=? WHERE generation=?";

    private ConstMobileNetworkQuery() {
    }
}
