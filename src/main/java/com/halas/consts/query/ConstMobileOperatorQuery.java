package com.halas.consts.query;

public class ConstMobileOperatorQuery {
    public static final String FIND_ALL =
            "SELECT code, name, country_code, mobile_network_generation FROM mobile_operator";
    public static final String FIND_BY_PK =
            "SELECT code, name, country_code, mobile_network_generation FROM mobile_operator WHERE code=?";
    public static final String DELETE_BY_PK = "DELETE FROM mobile_operator WHERE code=?";
    public static final String CREATE =
            "INSERT mobile_operator (code, name, country_code, mobile_network_generation) VALUES (?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE mobile_operator SET name=?, country_code=?, mobile_network_generation=? WHERE code=?";

    private ConstMobileOperatorQuery() {
    }
}
