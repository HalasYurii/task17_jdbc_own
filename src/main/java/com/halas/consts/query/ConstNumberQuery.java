package com.halas.consts.query;

public class ConstNumberQuery {
    public static final String FIND_ALL =
            "SELECT mobile, consumer_code, contract_code, pin_code, mobile_operator_code FROM number";
    public static final String FIND_BY_PK =
            "SELECT mobile, consumer_code, contract_code, pin_code, mobile_operator_code FROM number WHERE mobile=?";
    public static final String DELETE_BY_PK = "DELETE FROM number WHERE code=?";
    public static final String CREATE =
            "INSERT number (mobile, consumer_code, contract_code, pin_code, mobile_operator_code) VALUES (?, ?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE number SET consumer_code=?, contract_code=?, pin_code=?, mobile_operator_code=? WHERE mobile=?";

    private ConstNumberQuery() {
    }
}
