package com.halas.consts.query;

public class ConstPinCodeQuery {
    public static final String FIND_ALL =
            "SELECT code FROM pin_code";
    public static final String FIND_BY_PK =
            "SELECT code FROM pin_code WHERE code=?";
    public static final String DELETE_BY_PK = "DELETE FROM pin_code WHERE code=?";
    public static final String CREATE =
            "INSERT pin_code (code) VALUES (?)";
    public static final String UPDATE =
            "UPDATE pin_code SET code=?";

    private ConstPinCodeQuery() {
    }
}
