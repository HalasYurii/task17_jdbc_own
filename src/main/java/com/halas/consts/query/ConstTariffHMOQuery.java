package com.halas.consts.query;

public class ConstTariffHMOQuery {
    public static final String FIND_ALL =
            "SELECT tariff_code, mobile_operator_code FROM tariff_has_mobile_operator";
    public static final String FIND_BY_PK =
            "SELECT tariff_code, mobile_operator_code FROM tariff_has_mobile_operator WHERE tariff_code=?";
    public static final String DELETE_BY_PK =
            "DELETE FROM tariff_has_mobile_operator WHERE tariff_code=?";
    public static final String CREATE =
            "INSERT tariff_has_mobile_operator (tariff_code, mobile_operator_code) VALUES (?, ?)";
    public static final String UPDATE =
            "UPDATE tariff_has_mobile_operator SET mobile_operator_code=? WHERE tariff_code=?";

    private ConstTariffHMOQuery() {
    }
}
