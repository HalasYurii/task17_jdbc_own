package com.halas.consts.query;

public class ConstTariffQuery {
    public static final String FIND_ALL =
            "SELECT code, name, price, type_price, duration, internet, " +
                    "calls_similar_operator, calls_another_operator_min, sms FROM tariff";
    public static final String FIND_BY_PK =
            "SELECT code, name, price, type_price, duration, internet, calls_similar_operator, " +
                    "calls_another_operator_min, sms FROM tariff WHERE code=?";
    public static final String DELETE_BY_PK =
            "DELETE FROM tariff WHERE code=?";
    public static final String CREATE =
            "INSERT tariff (code, name, price, type_price, duration, internet, " +
                    "calls_similar_operator, calls_another_operator_min, sms) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
    public static final String UPDATE =
            "UPDATE tariff SET name=?, price=?, type_price=?, duration=?, internet=?," +
                    " calls_similar_operator=?, calls_another_operator_min=? sms=? WHERE code=?";

    private ConstTariffQuery() {
    }
}
