package com.halas.controller;

import java.sql.SQLException;

public interface RepositoryController {
    void showAll() throws SQLException;

    void showByPK() throws SQLException;

    void create() throws SQLException;

    void update() throws SQLException;

    void deleteByPK() throws SQLException;
}
