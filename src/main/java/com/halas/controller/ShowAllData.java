package com.halas.controller;

import com.halas.controller.impl.*;

import java.sql.SQLException;

public class ShowAllData {
    public void run() throws SQLException {
        new ConsumerController().showAll();
        new ContractController().showAll();
        new CountryController().showAll();
        new MobileNetworkController().showAll();
        new MobileOperatorController().showAll();
        new NumberController().showAll();
        new PinCodeController().showAll();
        new TariffController().showAll();
        new TariffHMOController().showAll();
    }
}
