package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.Consumer;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.ConsumerSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.controller.take.TakeInputtedData.*;

public class ConsumerController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public ConsumerController() {
        log = LogManager.getLogger(ConsumerController.class);
        controller = new ConsumerSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_CONSUMER);
        List<Consumer> consumers = controller.findAll();
        if (consumers.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            consumers.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_CONSUMER);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        Consumer consumers = (Consumer) controller.findByPK(pk);
        if (Objects.isNull(consumers)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(consumers + "\n");
        }
    }

    private Consumer setConsumerByInputted() {
        Consumer consumer = new Consumer();
        consumer.setCode(getInt(""));
        consumer.setName(getString(""));
        consumer.setSurname(getString(""));
        consumer.setDateBorn(getDate(""));
        consumer.setCountryBorn(getString(""));
        consumer.setMoney(getBigDecimal(""));
        consumer.setTypeMoney(getString(""));
        return consumer;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_CONSUMER);
            log.info(CREATE_INFO);
            Consumer consumer = setConsumerByInputted();
            int countRows = controller.create(consumer);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_CONSUMER);
        log.info(UPDATE_INFO);
        Consumer consumer = setConsumerByInputted();
        int countRows = controller.update(consumer);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_CONSUMER);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
