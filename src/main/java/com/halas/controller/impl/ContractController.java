package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.Contract;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.ContractSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.controller.take.TakeInputtedData.*;

public class ContractController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public ContractController() {
        log = LogManager.getLogger(ContractController.class);
        controller = new ContractSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_CONTRACT);
        List<Contract> contracts = controller.findAll();
        if (contracts.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            contracts.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_CONTRACT);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        Contract contract = (Contract) controller.findByPK(pk);
        if (Objects.isNull(contract)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(contract + "\n");
        }
    }

    private Contract setContractByInputted() {
        Contract contract = new Contract();
        contract.setCode(getInt(""));
        contract.setDateStart(getDate(""));
        contract.setDateEnd(getDate(""));
        contract.setTariffCode(getInt(""));
        return contract;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_CONTRACT);
            log.info(CREATE_INFO);
            Contract contract = setContractByInputted();
            int countRows = controller.create(contract);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_CONTRACT);
        log.info(UPDATE_INFO);
        Contract contract = setContractByInputted();
        int countRows = controller.update(contract);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_CONTRACT);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
