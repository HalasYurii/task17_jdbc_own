package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.Country;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.CountrySqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.*;

public class CountryController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public CountryController() {
        log = LogManager.getLogger(CountryController.class);
        controller = new CountrySqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_COUNTRY);
        List<Country> contracts = controller.findAll();
        if (contracts.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            contracts.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_COUNTRY);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        Country country = (Country) controller.findByPK(pk);
        if (Objects.isNull(country)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(country + "\n");
        }
    }

    private Country setCountryByInputted() {
        Country country = new Country();
        country.setCode(getInt(""));
        country.setName(getString(""));
        return country;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_COUNTRY);
            log.info(CREATE_INFO);
            Country country = setCountryByInputted();
            int countRows = controller.create(country);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_COUNTRY);
        log.info(UPDATE_INFO);
        Country country = setCountryByInputted();
        int countRows = controller.update(country);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_COUNTRY);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
