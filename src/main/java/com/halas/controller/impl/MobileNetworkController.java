package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.MobileNetwork;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.MobileNetworkSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.*;

public class MobileNetworkController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public MobileNetworkController() {
        log = LogManager.getLogger(MobileNetworkController.class);
        controller = new MobileNetworkSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_MOBILE_NET);
        List<MobileNetwork> mobileNetworks = controller.findAll();
        if (mobileNetworks.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            mobileNetworks.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_MOBILE_NET);
        log.info(FIND_INFO);
        String pk = getString(String.format(INPUT_PK, STRING_MSG));
        MobileNetwork mobileNetwork = (MobileNetwork) controller.findByPK(pk);
        if (Objects.isNull(mobileNetwork)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(mobileNetwork + "\n");
        }
    }

    private MobileNetwork setMobileNetworkByInputted() {
        MobileNetwork network = new MobileNetwork();
        network.setGeneration(getString(""));
        network.setSpeed(getDouble(""));
        network.setTypeSpeed(getString(""));
        network.setYearBorn(getInt(""));
        return network;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_MOBILE_NET);
            log.info(CREATE_INFO);
            MobileNetwork network = setMobileNetworkByInputted();
            int countRows = controller.create(network);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_MOBILE_NET);
        log.info(UPDATE_INFO);
        MobileNetwork network = setMobileNetworkByInputted();
        int countRows = controller.update(network);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_MOBILE_NET);
        log.info(DELETE_INFO);
        String pkDelete = getString(String.format(INPUT_PK, STRING_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
