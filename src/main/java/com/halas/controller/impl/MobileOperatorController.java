package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.MobileOperator;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.MobileOperatorSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.getInt;
import static com.halas.controller.take.TakeInputtedData.getString;

public class MobileOperatorController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public MobileOperatorController() {
        log = LogManager.getLogger(MobileOperatorController.class);
        controller = new MobileOperatorSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_MOBILE_OPER);
        List<MobileOperator> operators = controller.findAll();
        if (operators.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            operators.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_MOBILE_OPER);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        MobileOperator operator = (MobileOperator) controller.findByPK(pk);
        if (Objects.isNull(operator)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(operator + "\n");
        }
    }

    private MobileOperator setMobileOperatorByInputted() {
        MobileOperator operator = new MobileOperator();
        operator.setCode(getInt(""));
        operator.setName(getString(""));
        operator.setCountryCode(getInt(""));
        operator.setMobileNetworkGeneration(getString(""));
        return operator;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_MOBILE_OPER);
            log.info(CREATE_INFO);
            MobileOperator operator = setMobileOperatorByInputted();
            int countRows = controller.create(operator);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_MOBILE_OPER);
        log.info(UPDATE_INFO);
        MobileOperator operator = setMobileOperatorByInputted();
        int countRows = controller.update(operator);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_MOBILE_OPER);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
