package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.respository.Repository;
import com.halas.model.tables.Number;
import com.halas.respository.impl.sql.NumberSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.getInt;
import static com.halas.controller.take.TakeInputtedData.getString;

public class NumberController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public NumberController() {
        log = LogManager.getLogger(NumberController.class);
        controller = new NumberSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_NUMBER);
        List<Number> numbers = controller.findAll();
        if (numbers.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            numbers.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_NUMBER);
        log.info(FIND_INFO);
        String pk = getString(String.format(INPUT_PK, STRING_MSG));
        Number number = (Number) controller.findByPK(pk);
        if (Objects.isNull(number)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(number + "\n");
        }
    }

    private Number setNumberByInputted() {
        Number number = new Number();
        number.setMobile(getString(""));
        number.setConsumerCode(getInt(""));
        number.setContractCode(getInt(""));
        number.setPinCode(getString(""));
        number.setMobileOperatorCode(getInt(""));
        return number;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_NUMBER);
            log.info(CREATE_INFO);
            Number number = setNumberByInputted();
            int countRows = controller.create(number);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_NUMBER);
        log.info(UPDATE_INFO);
        Number number = setNumberByInputted();
        int countRows = controller.update(number);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_NUMBER);
        log.info(DELETE_INFO);
        String pkDelete = getString(String.format(INPUT_PK, STRING_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
