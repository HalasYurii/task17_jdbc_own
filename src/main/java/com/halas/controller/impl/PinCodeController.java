package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.PinCode;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.PinCodeSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.getString;

public class PinCodeController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public PinCodeController() {
        log = LogManager.getLogger(PinCodeController.class);
        controller = new PinCodeSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_PIN_CODE);
        List<PinCode> codes = controller.findAll();
        if (codes.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            codes.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_PIN_CODE);
        log.info(FIND_INFO);
        String pk = getString(String.format(INPUT_PK, STRING_MSG));
        PinCode code = (PinCode) controller.findByPK(pk);
        if (Objects.isNull(code)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(code + "\n");
        }
    }

    private PinCode setPinCodeByInputted() {
        PinCode code = new PinCode();
        code.setCode(getString(""));
        return code;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_PIN_CODE);
            log.info(CREATE_INFO);
            PinCode code = setPinCodeByInputted();
            int countRows = controller.create(code);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_PIN_CODE);
        log.info(UPDATE_INFO);
        PinCode code = setPinCodeByInputted();
        int countRows = controller.update(code);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_PIN_CODE);
        log.info(DELETE_INFO);
        String pkDelete = getString(String.format(INPUT_PK, STRING_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
