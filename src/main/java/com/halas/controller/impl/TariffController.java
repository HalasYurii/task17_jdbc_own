package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.Tariff;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.TariffSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.*;

public class TariffController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public TariffController() {
        log = LogManager.getLogger(TariffController.class);
        controller = new TariffSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_TARIFF);
        List<Tariff> tariffs = controller.findAll();
        if (tariffs.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            tariffs.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_TARIFF);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        Tariff tariff = (Tariff) controller.findByPK(pk);
        if (Objects.isNull(tariff)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(tariff + "\n");
        }
    }

    private Tariff setTariffByInputted() {
        Tariff tariff = new Tariff();
        tariff.setCode(getInt(""));
        tariff.setName(getString(""));
        tariff.setPrice(getBigDecimal(""));
        tariff.setTypePrice(getString(""));
        tariff.setDuration(getString(""));
        tariff.setInternet(getString(""));
        tariff.setCallsSimilarOperator(getString(""));
        tariff.setCallsAnotherOperatorMin(getInt(""));
        tariff.setSms(getInt(""));
        return tariff;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_TARIFF);
            log.info(CREATE_INFO);
            Tariff tariff = setTariffByInputted();
            int countRows = controller.create(tariff);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_TARIFF);
        log.info(UPDATE_INFO);
        Tariff tariff = setTariffByInputted();
        int countRows = controller.update(tariff);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_TARIFF);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
