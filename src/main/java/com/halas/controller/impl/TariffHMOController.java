package com.halas.controller.impl;

import com.halas.controller.RepositoryController;
import com.halas.model.tables.TariffHasMobileOperator;
import com.halas.respository.Repository;
import com.halas.respository.impl.sql.TariffHMOSqlRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.List;
import java.util.Objects;

import static com.halas.consts.ConstCommon.*;
import static com.halas.consts.ConstCommon.COUNT_DELETED_ROWS;
import static com.halas.controller.take.TakeInputtedData.getInt;

public class TariffHMOController implements RepositoryController {
    private Logger log;
    private Repository controller;

    public TariffHMOController() {
        log = LogManager.getLogger(TariffHMOController.class);
        controller = new TariffHMOSqlRepository();
    }

    @Override
    public void showAll() throws SQLException {
        log.info(TABLE_TARIFF_HMO);
        List<TariffHasMobileOperator> tariffs = controller.findAll();
        if (tariffs.isEmpty()) {
            log.error(EMPTY_TABLE);
        } else {
            tariffs.forEach(log::info);
        }
    }

    @Override
    public void showByPK() throws SQLException {
        log.info(TABLE_TARIFF);
        log.info(FIND_INFO);
        Integer pk = getInt(String.format(INPUT_PK, INTEGER_MSG));
        TariffHasMobileOperator tariff = (TariffHasMobileOperator) controller.findByPK(pk);
        if (Objects.isNull(tariff)) {
            log.error(BAD_PK_SHOW);
        } else {
            log.info(tariff + "\n");
        }
    }

    private TariffHasMobileOperator setTariffByInputted() {
        TariffHasMobileOperator tariff = new TariffHasMobileOperator();
        tariff.setTariffCode(getInt(""));
        tariff.setMobileOperatorCode(getInt(""));
        return tariff;
    }

    @Override
    public void create() throws SQLException {
        try {
            log.info(TABLE_TARIFF_HMO);
            log.info(CREATE_INFO);
            TariffHasMobileOperator tariff = setTariffByInputted();
            int countRows = controller.create(tariff);
            log.info(COUNT_CREATED_ROWS + countRows + "\n");
        } catch (SQLIntegrityConstraintViolationException e) {
            log.error(SQL_INTEGRITY_CONSTRAINT_VIOLATION_EXCEPTION);
        }
    }

    @Override
    public void update() throws SQLException {
        log.info(TABLE_TARIFF_HMO);
        log.info(UPDATE_INFO);
        TariffHasMobileOperator tariff = setTariffByInputted();
        int countRows = controller.update(tariff);
        log.info(COUNT_UPDATED_ROWS + countRows + "\n");
    }

    @Override
    public void deleteByPK() throws SQLException {
        log.info(TABLE_TARIFF_HMO);
        log.info(DELETE_INFO);
        Integer pkDelete = getInt(String.format(INPUT_PK, INTEGER_MSG));
        int countRows = controller.deleteByPK(pkDelete);
        log.info(COUNT_DELETED_ROWS + countRows + "\n");
    }
}
