package com.halas.controller.specifications;

import com.halas.respository.impl.specifications.NumberMobileOperatorConsumerAll;
import com.halas.respository.impl.sql.AdditionalSqlRepository;
import com.halas.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;

import static com.halas.consts.ConstMenu.SQL_EXCEPTION;

public class NumberMobOConsumerController implements Menu {
    private static final Logger LOG = LogManager.getLogger(NumberMobOConsumerController.class);

    @Override
    public void start() {
        try {
            new AdditionalSqlRepository().query(new NumberMobileOperatorConsumerAll());
        } catch (SQLException e) {
           LOG.error(SQL_EXCEPTION);
        }
    }
}
