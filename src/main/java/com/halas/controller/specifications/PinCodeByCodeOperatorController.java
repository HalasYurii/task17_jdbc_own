package com.halas.controller.specifications;

import com.halas.respository.impl.specifications.PinCodeByCodeOperator;
import com.halas.respository.impl.sql.PinCodeByCodeOperatorSqlRepository;
import com.halas.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.List;

import static com.halas.consts.ConstCommon.EMPTY_TABLE;
import static com.halas.consts.ConstMenu.SQL_EXCEPTION;
import static com.halas.consts.query.ConstAdditionalQuery.FAIL;
import static com.halas.consts.query.ConstAdditionalQuery.SUCCESS;
import static com.halas.controller.take.TakeInputtedData.getInt;
import static com.halas.controller.take.TakeInputtedData.getString;

public class PinCodeByCodeOperatorController implements Menu {
    private static final Logger LOG = LogManager.getLogger(PinCodeByCodeOperatorController.class);

    @Override
    public void start() {
        try {
            List<String> resultData =
                    new PinCodeByCodeOperatorSqlRepository(getString(""),getInt(""))
                    .query(new PinCodeByCodeOperator());
            if(resultData.isEmpty()){
                LOG.debug(FAIL);
            } else {
                LOG.debug(SUCCESS);
            }
        } catch (SQLException e) {
            LOG.error(SQL_EXCEPTION);
        }
    }
}
