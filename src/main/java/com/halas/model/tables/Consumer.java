package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

import java.math.BigDecimal;
import java.sql.Date;

@TableName(name = "consumer")
public class Consumer {
    @Pillar(name = "code")
    private Integer code;
    @Pillar(name = "name", length = 25)
    private String name;
    @Pillar(name = "surname", length = 35)
    private String surname;
    @Pillar(name = "date_born")
    private Date dateBorn;
    @Pillar(name = "country_born", length = 35)
    private String countryBorn;
    @Pillar(name = "money", length = 10)
    private BigDecimal money;
    @Pillar(name = "type_money", length = 15)
    private String typeMoney;

    public Consumer() {
    }


    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateBorn() {
        return dateBorn;
    }

    public void setDateBorn(Date dateBorn) {
        this.dateBorn = dateBorn;
    }

    public String getCountryBorn() {
        return countryBorn;
    }

    public void setCountryBorn(String countryBorn) {
        this.countryBorn = countryBorn;
    }

    public BigDecimal getMoney() {
        return money;
    }

    public void setMoney(BigDecimal money) {
        this.money = money;
    }

    public String getTypeMoney() {
        return typeMoney;
    }

    public void setTypeMoney(String typeMoney) {
        this.typeMoney = typeMoney;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-15s %-15s %-15s %-5f %-15s",
                code, name, surname, dateBorn, countryBorn, money, typeMoney);
    }
}
