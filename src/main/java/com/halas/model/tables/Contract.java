package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

import java.sql.Date;

@TableName(name = "contract")
public class Contract {
    @Pillar(name = "code")
    private Integer code;
    @Pillar(name = "date_start")
    private Date dateStart;
    @Pillar(name = "date_end")
    private Date dateEnd;
    @Pillar(name = "tariff_code")
    private Integer tariffCode;

    public Contract() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Integer getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(Integer tariffCode) {
        this.tariffCode = tariffCode;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-15s %-5d",
                code, dateStart, dateEnd, tariffCode);
    }
}
