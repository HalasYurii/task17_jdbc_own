package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "country")
public class Country {
    @Pillar(name = "code")
    private Integer code;
    @Pillar(name = "name", length = 35)
    private String name;

    public Country() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s", code, name);
    }
}
