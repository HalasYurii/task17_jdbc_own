package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "mobile_network")
public class MobileNetwork {
    @Pillar(name = "generation", length = 5)
    private String generation;
    @Pillar(name = "speed")
    private Double speed;
    @Pillar(name = "type_speed", length = 15)
    private String typeSpeed;
    @Pillar(name = "year_born", length = 4)
    private Integer yearBorn;

    public MobileNetwork() {
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getTypeSpeed() {
        return typeSpeed;
    }

    public void setTypeSpeed(String typeSpeed) {
        this.typeSpeed = typeSpeed;
    }

    public Integer getYearBorn() {
        return yearBorn;
    }

    public void setYearBorn(Integer yearBorn) {
        this.yearBorn = yearBorn;
    }

    @Override
    public String toString() {
        return String.format("%-5s %-8f %-15s %-4d",
                generation, speed, typeSpeed, yearBorn);
    }
}
