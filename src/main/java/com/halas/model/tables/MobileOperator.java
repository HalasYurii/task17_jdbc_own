package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "mobile_operator")
public class MobileOperator {
    @Pillar(name = "code")
    private Integer code;
    @Pillar(name = "name", length = 35)
    private String name;
    @Pillar(name = "country_code")
    private Integer countryCode;
    @Pillar(name = "mobile_network_generation", length = 5)
    private String mobileNetworkGeneration;


    public MobileOperator() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(Integer countryCode) {
        this.countryCode = countryCode;
    }

    public String getMobileNetworkGeneration() {
        return mobileNetworkGeneration;
    }

    public void setMobileNetworkGeneration(String mobileNetworkGeneration) {
        this.mobileNetworkGeneration = mobileNetworkGeneration;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-15s %-5d %-5s",
                code, name, countryCode, mobileNetworkGeneration);
    }
}
