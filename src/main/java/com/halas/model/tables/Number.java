package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "number")
public class Number {
    @Pillar(name = "mobile", length = 15)
    private String mobile;
    @Pillar(name = "consumer_code")
    private Integer consumerCode;
    @Pillar(name = "contract_code")
    private Integer contractCode;
    @Pillar(name = "pin_code", length = 8)
    private String pinCode;
    @Pillar(name = "mobile_operator_code")
    private Integer mobileOperatorCode;

    public Number() {
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getConsumerCode() {
        return consumerCode;
    }

    public void setConsumerCode(Integer consumerCode) {
        this.consumerCode = consumerCode;
    }

    public Integer getContractCode() {
        return contractCode;
    }

    public void setContractCode(Integer contractCode) {
        this.contractCode = contractCode;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public Integer getMobileOperatorCode() {
        return mobileOperatorCode;
    }

    public void setMobileOperatorCode(Integer mobileOperatorCode) {
        this.mobileOperatorCode = mobileOperatorCode;
    }

    @Override
    public String toString() {
        return String.format("%-15s %-5d %-5d %-8s %-5d",
                mobile, consumerCode, contractCode, pinCode, mobileOperatorCode);
    }
}
