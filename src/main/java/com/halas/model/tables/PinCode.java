package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "pin_code")
public class PinCode {
    @Pillar(name = "code", length = 8)
    private String code;

    public PinCode() {
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return String.format("%-8s", code);
    }
}
