package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

import java.math.BigDecimal;

@TableName(name = "tariff")
public class Tariff {
    @Pillar(name = "code")
    private Integer code;
    @Pillar(name = "name", length = 35)
    private String name;
    @Pillar(name = "price", length = 10)
    private BigDecimal price;
    @Pillar(name = "type_price", length = 15)
    private String typePrice;
    @Pillar(name = "duration", length = 10)
    private String duration;
    @Pillar(name = "internet", length = 15)
    private String internet;
    @Pillar(name = "calls_similar_operator", length = 25)
    private String callsSimilarOperator;
    @Pillar(name = "calls_another_operator_min")
    private Integer callsAnotherOperatorMin;
    @Pillar(name = "sms")
    private Integer sms;

    public Tariff() {
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTypePrice() {
        return typePrice;
    }

    public void setTypePrice(String typePrice) {
        this.typePrice = typePrice;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public String getInternet() {
        return internet;
    }

    public void setInternet(String internet) {
        this.internet = internet;
    }

    public String getCallsSimilarOperator() {
        return callsSimilarOperator;
    }

    public void setCallsSimilarOperator(String callsSimilarOperator) {
        this.callsSimilarOperator = callsSimilarOperator;
    }

    public Integer getCallsAnotherOperatorMin() {
        return callsAnotherOperatorMin;
    }

    public void setCallsAnotherOperatorMin(Integer callsAnotherOperatorMin) {
        this.callsAnotherOperatorMin = callsAnotherOperatorMin;
    }

    public Integer getSms() {
        return sms;
    }

    public void setSms(Integer sms) {
        this.sms = sms;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-35s %-10f %-25s %-10s %-18s %-25s %-5d %-5d"
                , code, name, price, typePrice, duration, internet, callsSimilarOperator, callsAnotherOperatorMin, sms);
    }
}
