package com.halas.model.tables;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

@TableName(name = "tariff_has_mobile_operator")
public class TariffHasMobileOperator {
    @Pillar(name = "tariff_code")
    private Integer tariffCode;
    @Pillar(name = "mobile_operator_code")
    private Integer mobileOperatorCode;

    public TariffHasMobileOperator() {
    }

    public Integer getTariffCode() {
        return tariffCode;
    }

    public void setTariffCode(Integer tariffCode) {
        this.tariffCode = tariffCode;
    }

    public Integer getMobileOperatorCode() {
        return mobileOperatorCode;
    }

    public void setMobileOperatorCode(Integer mobileOperatorCode) {
        this.mobileOperatorCode = mobileOperatorCode;
    }

    @Override
    public String toString() {
        return String.format("%-5d %-5d", tariffCode, mobileOperatorCode);
    }
}
