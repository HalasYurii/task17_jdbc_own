package com.halas.respository;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Collections;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;

public interface QueryRepository {
    default List<String> query(Specification specification) throws SQLException {
        final Logger log = LogManager.getLogger(QueryRepository.class);
        final SQLSpecification sqlSpecification = (SQLSpecification) specification;
        try (Statement statement = CONNECTION.createStatement()) {
            try (ResultSet resultSet = statement.executeQuery(sqlSpecification.toSQLQuery())) {
                while (resultSet.next()) {
                    StringBuilder rowResult = new StringBuilder();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                        rowResult.append(String.format("%-25s", resultSet.getString(i)));
                    }
                    log.info(rowResult);
                }
            }
        }
        return Collections.emptyList();
    }
}
