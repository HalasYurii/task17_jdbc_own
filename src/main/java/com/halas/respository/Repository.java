package com.halas.respository;

import java.sql.SQLException;
import java.util.List;

public interface Repository<T, ID> extends QueryRepository {
    List<T> findAll() throws SQLException;

    T findByPK(ID id) throws SQLException;

    int create(T entity) throws SQLException;

    int update(T entity) throws SQLException;

    int deleteByPK(ID id) throws SQLException;
}
