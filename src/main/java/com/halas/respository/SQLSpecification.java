package com.halas.respository;

public interface SQLSpecification extends Specification {
    String toSQLQuery();
}
