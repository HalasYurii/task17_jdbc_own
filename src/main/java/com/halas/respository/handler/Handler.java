package com.halas.respository.handler;

import com.halas.model.annot.Pillar;
import com.halas.model.annot.TableName;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Handler<T> {
    private final Class<T> clazz;

    public Handler(Class<T> clazz) {
        this.clazz = clazz;
    }

    private void setField(Object resultObject, ResultSet resultSet, String name, Field field)
            throws SQLException, IllegalAccessException {
        Class fieldType = field.getType();
        if (fieldType == String.class) {
            field.set(resultObject, resultSet.getString(name));
        } else if (fieldType == Integer.class) {
            field.set(resultObject, resultSet.getInt(name));
        } else if (fieldType == Date.class) {
            field.set(resultObject, resultSet.getDate(name));
        } else if (fieldType == BigDecimal.class) {
            field.set(resultObject, resultSet.getBigDecimal(name));
        } else if (fieldType == Double.class) {
            field.set(resultObject, resultSet.getDouble(name));
        }
    }

    public Object fromResultSetToEntity(ResultSet resultSet) throws SQLException {
        Object resultObject = new Object();
        try {
            resultObject = clazz.getConstructor().newInstance();
            if (clazz.isAnnotationPresent(TableName.class)) {
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Pillar.class)) {
                        Pillar column = field.getAnnotation(Pillar.class);
                        String nameColumn = column.name();
                        field.setAccessible(true);
                        setField(resultObject, resultSet, nameColumn, field);
                    }
                }
            }
        } catch (InstantiationException
                | IllegalAccessException
                | InvocationTargetException
                | NoSuchMethodException ignored) {
        }
        return resultObject;
    }
}