package com.halas.respository.impl.specifications;

import com.halas.respository.SQLSpecification;

import static com.halas.consts.query.ConstAdditionalQuery.NUMBER_MOBILE_OPERATOR_CONSUMER_ALL;

public class NumberMobileOperatorConsumerAll implements SQLSpecification {
    @Override
    public String toSQLQuery() {
        return NUMBER_MOBILE_OPERATOR_CONSUMER_ALL;
    }
}
