package com.halas.respository.impl.specifications;

import com.halas.respository.SQLSpecification;

import static com.halas.consts.query.ConstAdditionalQuery.PIN_CODE_AND_CODE_OPERATOR;

public class PinCodeByCodeOperator implements SQLSpecification {
    @Override
    public String toSQLQuery() {
        return PIN_CODE_AND_CODE_OPERATOR;
    }
}
