package com.halas.respository.impl.sql;

import com.halas.model.tables.Consumer;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstConsumerQuery.*;

public class ConsumerSqlRepository
        implements Repository<Consumer, Integer> {
    @Override
    public List<Consumer> findAll() throws SQLException {
        List<Consumer> consumers = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL_CONSUMER)) {
            while (resultSet.next()) {
                consumers.add((Consumer) new Handler(Consumer.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return consumers;
    }

    @Override
    public Consumer findByPK(Integer pk) throws SQLException {
        Consumer consumer = new Consumer();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setInt(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    consumer = (Consumer) new Handler(Consumer.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return consumer;
    }

    @Override
    public int create(Consumer entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setString(3, entity.getSurname());
            preparedStatement.setDate(4, entity.getDateBorn());
            preparedStatement.setString(5, entity.getCountryBorn());
            preparedStatement.setBigDecimal(6, entity.getMoney());
            preparedStatement.setString(7, entity.getTypeMoney());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Consumer entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSurname());
            preparedStatement.setDate(3, entity.getDateBorn());
            preparedStatement.setString(4, entity.getCountryBorn());
            preparedStatement.setBigDecimal(5, entity.getMoney());
            preparedStatement.setString(6, entity.getTypeMoney());
            preparedStatement.setInt(7, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
