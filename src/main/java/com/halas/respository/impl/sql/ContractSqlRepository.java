package com.halas.respository.impl.sql;

import com.halas.model.tables.Contract;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstContractQuery.*;

public class ContractSqlRepository
        implements Repository<Contract, Integer> {

    @Override
    public List<Contract> findAll() throws SQLException {
        List<Contract> contracts = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                contracts.add((Contract) new Handler(Contract.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return contracts;
    }

    @Override
    public Contract findByPK(Integer pk) throws SQLException {
        Contract contract = new Contract();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setInt(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    contract = (Contract) new Handler(Contract.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return contract;
    }

    @Override
    public int create(Contract entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getCode());
            preparedStatement.setDate(2, entity.getDateStart());
            preparedStatement.setDate(3, entity.getDateEnd());
            preparedStatement.setInt(4, entity.getTariffCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Contract entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setDate(1, entity.getDateStart());
            preparedStatement.setDate(2, entity.getDateEnd());
            preparedStatement.setInt(3, entity.getTariffCode());
            preparedStatement.setInt(4, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
