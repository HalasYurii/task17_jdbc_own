package com.halas.respository.impl.sql;

import com.halas.model.tables.Country;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstCountryQuery.*;

public class CountrySqlRepository
        implements Repository<Country, Integer> {

    @Override
    public List<Country> findAll() throws SQLException {
        List<Country> countries = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                countries.add((Country) new Handler(Country.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return countries;
    }

    @Override
    public Country findByPK(Integer pk) throws SQLException {
        Country entity = new Country();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setInt(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery(FIND_BY_PK)) {
                if (resultSet.next()) {
                    entity = (Country) new Handler(Country.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return entity;
    }

    @Override
    public int create(Country entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Country entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
