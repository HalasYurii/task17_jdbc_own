package com.halas.respository.impl.sql;

import com.halas.model.tables.MobileNetwork;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstMobileNetworkQuery.*;

public class MobileNetworkSqlRepository
        implements Repository<MobileNetwork, String> {

    @Override
    public List<MobileNetwork> findAll() throws SQLException {
        List<MobileNetwork> mobileNetworks = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                mobileNetworks.add((MobileNetwork) new Handler(MobileNetwork.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return mobileNetworks;
    }

    @Override
    public MobileNetwork findByPK(String pk) throws SQLException {
        MobileNetwork mobileNetwork = new MobileNetwork();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setString(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    mobileNetwork = (MobileNetwork) new Handler(MobileNetwork.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return mobileNetwork;
    }

    @Override
    public int create(MobileNetwork entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setString(1, entity.getGeneration());
            preparedStatement.setDouble(2, entity.getSpeed());
            preparedStatement.setString(3, entity.getTypeSpeed());
            preparedStatement.setInt(4, entity.getYearBorn());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(MobileNetwork entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setDouble(1, entity.getSpeed());
            preparedStatement.setString(2, entity.getTypeSpeed());
            preparedStatement.setInt(3, entity.getYearBorn());
            preparedStatement.setString(4, entity.getGeneration());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(String pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setString(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
