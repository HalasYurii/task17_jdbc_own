package com.halas.respository.impl.sql;

import com.halas.model.tables.MobileOperator;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstMobileOperatorQuery.*;

public class MobileOperatorSqlRepository
        implements Repository<MobileOperator, Integer> {

    @Override
    public List<MobileOperator> findAll() throws SQLException {
        List<MobileOperator> mobileOperators = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                mobileOperators.add((MobileOperator) new Handler(MobileOperator.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return mobileOperators;
    }

    @Override
    public MobileOperator findByPK(Integer pk) throws SQLException {
        MobileOperator mobileOperator = new MobileOperator();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setInt(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    mobileOperator = (MobileOperator) new Handler(MobileOperator.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return mobileOperator;
    }

    @Override
    public int create(MobileOperator entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setInt(3, entity.getCountryCode());
            preparedStatement.setString(4, entity.getMobileNetworkGeneration());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(MobileOperator entity) throws SQLException {
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setInt(2, entity.getCountryCode());
            preparedStatement.setString(3, entity.getMobileNetworkGeneration());
            preparedStatement.setInt(4, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
