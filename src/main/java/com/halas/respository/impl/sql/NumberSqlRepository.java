package com.halas.respository.impl.sql;

import com.halas.model.tables.Number;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstNumberQuery.*;

public class NumberSqlRepository
        implements Repository<Number, String> {

    @Override
    public List<Number> findAll() throws SQLException {
        List<Number> numbers = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                numbers.add((Number) new Handler(Number.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return numbers;
    }

    @Override
    public Number findByPK(String pk) throws SQLException {
        Number number = new Number();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setString(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    number = (Number) new Handler(Number.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return number;
    }

    @Override
    public int create(Number entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setString(1, entity.getMobile());
            preparedStatement.setInt(2, entity.getConsumerCode());
            preparedStatement.setInt(3, entity.getContractCode());
            preparedStatement.setString(4, entity.getPinCode());
            preparedStatement.setInt(5, entity.getMobileOperatorCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Number entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setInt(1, entity.getConsumerCode());
            preparedStatement.setInt(2, entity.getContractCode());
            preparedStatement.setString(3, entity.getPinCode());
            preparedStatement.setInt(4, entity.getMobileOperatorCode());
            preparedStatement.setString(5, entity.getMobile());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(String pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setString(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
