package com.halas.respository.impl.sql;

import com.halas.respository.QueryRepository;
import com.halas.respository.SQLSpecification;
import com.halas.respository.Specification;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;

public class PinCodeByCodeOperatorSqlRepository implements QueryRepository {
    private String pinCode;
    private int codeOperator;

    public PinCodeByCodeOperatorSqlRepository(String pinCode, int codeOperator) {
        this.pinCode = pinCode;
        this.codeOperator = codeOperator;
    }

    @Override
    public List<String> query(Specification specification) throws SQLException {
        final SQLSpecification sqlSpecification = (SQLSpecification) specification;
        List<String> resultData = new ArrayList<>();
        try (PreparedStatement preparedStatement = CONNECTION.prepareStatement(sqlSpecification.toSQLQuery())) {
            preparedStatement.setString(1, pinCode);
            preparedStatement.setInt(2, codeOperator);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                        resultData.add(resultSet.getString(i));
                    }
                }
            }
        }
        return resultData;
    }
}
