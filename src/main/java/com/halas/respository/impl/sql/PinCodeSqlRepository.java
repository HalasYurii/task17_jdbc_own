package com.halas.respository.impl.sql;

import com.halas.model.tables.PinCode;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstPinCodeQuery.*;

public class PinCodeSqlRepository
        implements Repository<PinCode, String> {

    @Override
    public List<PinCode> findAll() throws SQLException {
        List<PinCode> pinCodes = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                pinCodes.add((PinCode) new Handler(PinCode.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return pinCodes;
    }

    @Override
    public PinCode findByPK(String pk) throws SQLException {
        PinCode pinCode = new PinCode();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setString(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    pinCode = (PinCode) new Handler(PinCode.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return pinCode;
    }

    @Override
    public int create(PinCode entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setString(1, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(PinCode entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(String pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setString(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
