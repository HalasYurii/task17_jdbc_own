package com.halas.respository.impl.sql;

import com.halas.model.tables.TariffHasMobileOperator;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstTariffHMOQuery.*;

public class TariffHMOSqlRepository
        implements Repository<TariffHasMobileOperator, Integer> {

    @Override
    public List<TariffHasMobileOperator> findAll() throws SQLException {
        List<TariffHasMobileOperator> tariffs = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                tariffs.add((TariffHasMobileOperator) new Handler(
                        TariffHasMobileOperator.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return tariffs;
    }

    @Override
    public TariffHasMobileOperator findByPK(Integer pk) throws SQLException {
        TariffHasMobileOperator tariff = new TariffHasMobileOperator();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            preparedStatement.setInt(1, pk);
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    tariff = (TariffHasMobileOperator) new Handler(
                            TariffHasMobileOperator.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return tariff;
    }

    @Override
    public int create(TariffHasMobileOperator entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getTariffCode());
            preparedStatement.setInt(2, entity.getMobileOperatorCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(TariffHasMobileOperator entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setInt(2, entity.getTariffCode());
            preparedStatement.setInt(1, entity.getMobileOperatorCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
