package com.halas.respository.impl.sql;

import com.halas.model.tables.Tariff;
import com.halas.respository.Repository;
import com.halas.respository.handler.Handler;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import static com.halas.consts.ConstConnection.CONNECTION;
import static com.halas.consts.query.ConstTariffQuery.*;

public class TariffSqlRepository
        implements Repository<Tariff, Integer> {

    @Override
    public List<Tariff> findAll() throws SQLException {
        List<Tariff> tariffs = new ArrayList<>();
        try (Statement statement = CONNECTION.createStatement();
             ResultSet resultSet = statement.executeQuery(FIND_ALL)) {
            while (resultSet.next()) {
                tariffs.add((Tariff) new Handler(Tariff.class)
                        .fromResultSetToEntity(resultSet));
            }
        }
        return tariffs;
    }

    @Override
    public Tariff findByPK(Integer pk) throws SQLException {
        Tariff tariff = new Tariff();
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(FIND_BY_PK)) {
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                if (resultSet.next()) {
                    tariff = (Tariff) new Handler(Tariff.class)
                            .fromResultSetToEntity(resultSet);
                }
            }
        }
        return tariff;
    }

    @Override
    public int create(Tariff entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(CREATE)) {
            preparedStatement.setInt(1, entity.getCode());
            preparedStatement.setString(2, entity.getName());
            preparedStatement.setBigDecimal(3, entity.getPrice());
            preparedStatement.setString(4, entity.getTypePrice());
            preparedStatement.setString(5, entity.getDuration());
            preparedStatement.setString(6, entity.getInternet());
            preparedStatement.setString(7, entity.getCallsSimilarOperator());
            preparedStatement.setInt(8, entity.getCallsAnotherOperatorMin());
            preparedStatement.setInt(9, entity.getSms());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int update(Tariff entity) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(UPDATE)) {
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setBigDecimal(2, entity.getPrice());
            preparedStatement.setString(3, entity.getTypePrice());
            preparedStatement.setString(4, entity.getDuration());
            preparedStatement.setString(5, entity.getInternet());
            preparedStatement.setString(6, entity.getCallsSimilarOperator());
            preparedStatement.setInt(7, entity.getCallsAnotherOperatorMin());
            preparedStatement.setInt(8, entity.getSms());
            preparedStatement.setInt(9, entity.getCode());
            return preparedStatement.executeUpdate();
        }
    }

    @Override
    public int deleteByPK(Integer pk) throws SQLException {
        try (PreparedStatement preparedStatement =
                     CONNECTION.prepareStatement(DELETE_BY_PK)) {
            preparedStatement.setInt(1, pk);
            return preparedStatement.executeUpdate();
        }
    }
}
