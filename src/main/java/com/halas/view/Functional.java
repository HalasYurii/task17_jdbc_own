package com.halas.view;

import java.sql.SQLException;

@FunctionalInterface
public interface Functional {
    void execute() throws SQLException;
}
