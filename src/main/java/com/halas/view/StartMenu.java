package com.halas.view;

import com.halas.controller.ShowAllData;
import com.halas.controller.specifications.NumberMobOConsumerController;
import com.halas.controller.specifications.PinCodeByCodeOperatorController;
import com.halas.view.submenu.concrete.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.consts.ConstCommon.INPUT;
import static com.halas.consts.ConstMenu.*;
import static com.halas.view.checks.MethodsMenu.isCorrect;

public class StartMenu implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Functional> menuMethods;
    private Logger log;

    public StartMenu() {
        log = LogManager.getLogger(StartMenu.class);
        initMenu();
    }

    private void initDescriptionsMenu() {
        menuDescription.put("1", " 1 - Show data from all tables.");
        menuDescription.put("2", " 2 - Show all data from join three " +
                "tables \'Consumer\',\'Number\',\'Mobile operator\'.");
        menuDescription.put("3", " 3 - Try to guess pin code and operator.");
        menuDescription.put("4", " 4 - Go to table: \'Consumer\'.");
        menuDescription.put("5", " 5 - Go to table: \'Contract\'.");
        menuDescription.put("6", " 6 - Go to table: \'Country\'.");
        menuDescription.put("7", " 7 - Go to table: \'Mobile network\'.");
        menuDescription.put("8", " 8 - Go to table: \'Mobile operator\'.");
        menuDescription.put("9", " 9 - Go to table: \'Number\'.");
        menuDescription.put("10", " 10 - Go to table: \'Pin code\'.");
        menuDescription.put("11", " 11 - Go to table: \'Tariff has mobile operator\'.");
        menuDescription.put("12", " 12 - Go to table: \'Tariff\'.");
        menuDescription.put(EXIT_POINT, String.format(" %s - Exit.", EXIT_POINT));
    }

    private void initMethodsMenu() {
        menuMethods.put("1", new ShowAllData()::run);
        menuMethods.put("2", new NumberMobOConsumerController()::start);
        menuMethods.put("3", new PinCodeByCodeOperatorController()::start);
        menuMethods.put("4", new ConsumerMenu()::start);
        menuMethods.put("5", new ContractMenu()::start);
        menuMethods.put("6", new CountryMenu()::start);
        menuMethods.put("7", new MobileNetworkMenu()::start);
        menuMethods.put("8", new MobileOperatorMenu()::start);
        menuMethods.put("9", new NumberMenu()::start);
        menuMethods.put("10", new PinCodeMenu()::start);
        menuMethods.put("11", new TariffHMOMenu()::start);
        menuMethods.put("12", new TariffMenu()::start);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initDescriptionsMenu();
        initMethodsMenu();
    }

    private void outputMenu() {
        log.debug(MENU_START);
        menuDescription.values().forEach(log::info);
    }

    @Override
    public void start() {
        String key;
        int sizeMenu = menuMethods.size();
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                if (isCorrect(key, sizeMenu)) {
                    log.info("\n\n");
                    if (key.equals(EXIT_POINT)) {
                        break;
                    }
                    menuMethods.get(key).execute();
                } else {
                    log.error(TRY_AGAIN);
                }
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            }
        }
    }
}
