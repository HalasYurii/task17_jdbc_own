package com.halas.view.checks;

public class MethodsMenu {
    private static boolean isInt(String str) {
        return str.matches("^\\d+$");
    }

    public static boolean isCorrect(String key, int sizeMenu) {
        return isInt(key) &&
                Integer.parseInt(key) <= sizeMenu &&
                Integer.parseInt(key) >= 0;
    }
}
