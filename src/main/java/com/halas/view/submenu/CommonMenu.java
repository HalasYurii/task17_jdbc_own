package com.halas.view.submenu;

import com.halas.controller.RepositoryController;
import com.halas.view.Functional;
import com.halas.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.halas.consts.ConstCommon.INPUT;
import static com.halas.consts.ConstMenu.*;
import static com.halas.consts.ConstMenu.SQL_EXCEPTION;
import static com.halas.view.checks.MethodsMenu.isCorrect;

public class CommonMenu implements Menu {
    private Map<String, String> menuDescription;
    private Map<String, Functional> menuMethods;
    private RepositoryController controller;
    private String nameTable;
    private Logger log;

    public CommonMenu(RepositoryController controller, String nameTable) {
        this.controller = controller;
        this.nameTable = nameTable;
        this.log = LogManager.getLogger(controller.getClass());
        initMenu();
    }

    private void initMenuDescription() {
        menuDescription.put("1", " 1 - Show all data.");
        menuDescription.put("2", " 2 - Find by PK.");
        menuDescription.put("3", " 3 - Create data in current table.");
        menuDescription.put("4", " 4 - Update data in current table.");
        menuDescription.put("5", " 5 - Delete by PK.");
        menuDescription.put(BACK_POINT, String.format(" %s - Back to main menu.", BACK_POINT));
    }

    private void initMenuMethods() {
        menuMethods.put("1", controller::showAll);
        menuMethods.put("2", controller::showByPK);
        menuMethods.put("3", controller::create);
        menuMethods.put("4", controller::update);
        menuMethods.put("5", controller::deleteByPK);
    }

    private void initMenu() {
        menuDescription = new LinkedHashMap<>();
        menuMethods = new LinkedHashMap<>();
        initMenuDescription();
        initMenuMethods();
    }

    private void outputMenu() {
        log.debug(nameTable);
        menuDescription.values().forEach(log::info);
    }

    private void checkAndRun(String key, int sizeMenu)
            throws SQLException {
        if (isCorrect(key, sizeMenu)) {
            log.info("\n\n");
            menuMethods.get(key).execute();
        } else {
            log.error(TRY_AGAIN);
        }
    }

    @Override
    public void start() {
        String key;
        int sizeMenu = menuMethods.size();
        while (true) {
            try {
                log.info("\n");
                outputMenu();
                log.info(SELECT_MSG);
                key = INPUT.nextLine();
                if (key.equals(BACK_POINT)) {
                    break;
                }
                checkAndRun(key, sizeMenu);
            } catch (SQLException e) {
                log.error(SQL_EXCEPTION);
            }
        }
    }
}
