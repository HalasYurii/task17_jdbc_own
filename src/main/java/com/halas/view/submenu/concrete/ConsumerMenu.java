package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.ConsumerController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_CONSUMER;

public class ConsumerMenu implements Menu {
    @Override
    public void start() {
        RepositoryController consumerController = new ConsumerController();
        new CommonMenu(consumerController, MENU_CONSUMER).start();
    }
}
