package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.ContractController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_CONTRACT;

public class ContractMenu implements Menu {
    @Override
    public void start() {
        RepositoryController contractController = new ContractController();
        new CommonMenu(contractController, MENU_CONTRACT).start();
    }
}
