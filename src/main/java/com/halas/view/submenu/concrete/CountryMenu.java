package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.CountryController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_COUNTRY;

public class CountryMenu implements Menu {
    @Override
    public void start() {
        RepositoryController countryController = new CountryController();
        new CommonMenu(countryController, MENU_COUNTRY).start();
    }
}
