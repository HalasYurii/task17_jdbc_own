package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.MobileNetworkController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_MOBILE_NETWORK;

public class MobileNetworkMenu implements Menu {
    @Override
    public void start() {
        RepositoryController mobileNetworkController =
                new MobileNetworkController();
        new CommonMenu(mobileNetworkController, MENU_MOBILE_NETWORK).start();
    }
}
