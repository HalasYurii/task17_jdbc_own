package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.MobileOperatorController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_MOBILE_OPERATOR;

public class MobileOperatorMenu implements Menu {
    @Override
    public void start() {
        RepositoryController controller = new MobileOperatorController();
        new CommonMenu(controller, MENU_MOBILE_OPERATOR).start();
    }
}
