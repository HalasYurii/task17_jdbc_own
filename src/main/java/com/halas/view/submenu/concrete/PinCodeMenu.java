package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.PinCodeController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_PIN_CODE;

public class PinCodeMenu implements Menu {
    @Override
    public void start() {
        RepositoryController controller = new PinCodeController();
        new CommonMenu(controller, MENU_PIN_CODE).start();
    }
}
