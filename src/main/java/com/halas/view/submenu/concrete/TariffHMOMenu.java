package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.TariffHMOController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_TARIFF_HMO;

public class TariffHMOMenu implements Menu {
    @Override
    public void start() {
        RepositoryController controller = new TariffHMOController();
        new CommonMenu(controller, MENU_TARIFF_HMO).start();
    }
}
