package com.halas.view.submenu.concrete;

import com.halas.controller.RepositoryController;
import com.halas.controller.impl.TariffController;
import com.halas.view.Menu;
import com.halas.view.submenu.CommonMenu;

import static com.halas.consts.ConstMenu.MENU_TARIFF;

public class TariffMenu implements Menu {
    @Override
    public void start() {
        RepositoryController controller = new TariffController();
        new CommonMenu(controller, MENU_TARIFF).start();
    }
}
